import 'package:flutter/material.dart';
import 'package:flutter_app/pages/homePage.dart';

void main() => runApp(App());

class App extends StatefulWidget{
  @override
  AppState createState() => AppState();
}

class AppState extends State<App>{
  @override
  Widget build(BuildContext context) {
    return (
        MaterialApp(
          debugShowCheckedModeBanner: false,
          title: "Drive Time",
          home: HomeScreen(),
        ));
  }

}