import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HomeScreen extends StatefulWidget {

  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  var isFirstTimeLoading = true;

  @override
  // ignore: must_call_super
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.white,
            ),
            Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  child: Column(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage('assets/header.png'),
                                fit: BoxFit.cover)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 30, bottom: 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Image(image: AssetImage('assets/book.png')),
                              ),
                              Text(
                                  'DriveTime',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'OpenSans',
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(top: 30, bottom: 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        'Select Vehicle',
                                        style: TextStyle(
                                          color: Colors.black87,
                                          fontFamily: 'OpenSans',
                                          fontSize: 12.0,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 4.0),
                                        child: Image(image: AssetImage('assets/arrow.png')),
                                      ),
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 16.0),
                                    child: Text(
                                      '114 000km',
                                      style: TextStyle(
                                        color: Colors.black87,
                                        fontFamily: 'OpenSans',
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    'Hubometer',
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontFamily: 'OpenSans',
                                      fontSize: 12.0,
                                    ),
                                  ),
                                ],
                              ),
                              Container(height: 80, child: VerticalDivider(color: Colors.black54)),
                              Column(
                                children: [
                                  Text(
                                    'IDLE',
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontFamily: 'OpenSans',
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold
                                    ),
                                  ),
                                  Text(
                                    'Currant Status',
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontFamily: 'OpenSans',
                                      fontSize: 12.0,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 4.0),
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(right: 4.0),
                                          child: Image(image: AssetImage('assets/star.png')),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(right: 4.0),
                                          child: Image(image: AssetImage('assets/star.png')),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(right: 4.0),
                                          child: Image(image: AssetImage('assets/star.png')),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(right: 4.0),
                                          child: Image(image: AssetImage('assets/star.png')),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(right: 4.0),
                                          child: Image(image: AssetImage('assets/empty.png')),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              Container(height: 80, child: VerticalDivider(color: Colors.black54)),
                              Column(
                                children: [
                                  Text(
                                    '3h',
                                    style: TextStyle(
                                        color: Colors.black87,
                                        fontFamily: 'OpenSans',
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                  Text(
                                    'Next Break due',
                                    style: TextStyle(
                                      color: Colors.black87,
                                      fontFamily: 'OpenSans',
                                      fontSize: 12.0,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      isFirstTimeLoading ? Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: RaisedButton(
                          elevation: 5.0,
                          onPressed: (){
                            setState(() {
                              isFirstTimeLoading = false;
                            });
                            showAlertDialog(context);
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: Colors.green[100],
                          child: Padding(
                            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                            child: Container(
                              decoration: new BoxDecoration(
                                  color: Colors.green[100],
                                  borderRadius: new BorderRadius.all(Radius.circular(10))
                              ),
                              width: double.infinity,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                                    child: Image(image: AssetImage('assets/play.png')),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 16.0),
                                    child: Text(
                                      'START DRIVING',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'OpenSans',
                                        fontSize: 25.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ) : Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: RaisedButton(
                          elevation: 5.0,
                          onPressed: (){
                            setState(() {
                              isFirstTimeLoading = false;
                            });
                            showAlertDialog(context);
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: Colors.green[100],
                          child: Padding(
                            padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                            child: Container(
                              decoration: new BoxDecoration(
                                  color: Colors.green[100],
                                  borderRadius: new BorderRadius.all(Radius.circular(10))
                              ),
                              width: double.infinity,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Text(
                                      'Time & Date : '+timeDateBox.text,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'OpenSans',
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Text(
                                      'Location : '+locationBox.text,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'OpenSans',
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child: Text(
                                      'Hubo Reading : '+hoboBox.text,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: 'OpenSans',
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                            child: RaisedButton(
                              elevation: 5.0,
                              onPressed: (){
                                setState(() {
                                });
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              color: Colors.yellow,
                              child: Container(
                                width: MediaQuery. of(context). size. width/2.8,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                                      child: Image(image: AssetImage('assets/work.png')),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 16.0),
                                      child: Text(
                                        'START OTHER WORK',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: 'OpenSans',
                                          fontSize: 15.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0, top: 16.0),
                            child: RaisedButton(
                              elevation: 5.0,
                              onPressed: (){
                                setState(() {
                                });
                              },
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              color: Colors.lightBlueAccent,
                              child: Container(
                                width: MediaQuery. of(context). size. width/2.8,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                                      child: Image(image: AssetImage('assets/rest.png')),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(bottom: 16.0),
                                      child: Text(
                                        'START REST',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: 'OpenSans',
                                          fontSize: 15.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                        child: RaisedButton(
                          elevation: 5.0,
                          onPressed: (){
                            setState(() {
                            });
                          },
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          color: Colors.redAccent,
                          child: Container(
                            width: double.infinity,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
                                  child: Image(image: AssetImage('assets/end.png')),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 16.0),
                                  child: Text(
                                    'END OF DAY',
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontFamily: 'OpenSans',
                                      fontSize: 25.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0, left: 16.0),
                        child: Row(
                          mainAxisAlignment:  MainAxisAlignment.start,
                          children: [
                            Text(
                              "Driver's Last Remark:",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'OpenSans',
                                fontSize: 15.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16.0),
                        child: Row(
                          mainAxisAlignment:  MainAxisAlignment.start,
                          children: [
                            Text(
                              "Gate access code is 8237452. ask for Mike",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'OpenSans',
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(width: MediaQuery. of(context). size. width/4, child: Image(image: AssetImage('assets/truck.png'))),
                          Container(width: MediaQuery. of(context). size. width/4, child: Image(image: AssetImage('assets/share.png'))),
                          Container(width: MediaQuery. of(context). size. width/4, child: Image(image: AssetImage('assets/book2.png'))),
                          Container(width: MediaQuery. of(context). size. width/4, child: Image(image: AssetImage('assets/settings.png'))),
                        ],
                      ),
                    ],
                  ),
                )

            )
          ],
        )
    );
  }

  showAlertDialog(BuildContext context) {

    // set up the button
    Widget okButton = FlatButton(
      child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery. of(context). size. width,
                  decoration: new BoxDecoration(
                    color: Colors.deepOrange,
                    borderRadius: new BorderRadius.all(new Radius.circular(32)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("CONFIRM & LOG",
                               style: TextStyle(
                                 color: Colors.white
                               ),
                        ),
                      ],
                    ),
                  )
              ),
              Container(
                  width: MediaQuery. of(context). size. width,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("CANCEL",
                          style: TextStyle(
                              color: Colors.deepOrange
                          ),
                        ),
                      ],
                    ),
                  )
              ),
            ],
          )),
      onPressed: () {
        Navigator.pop(context, true);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Start Driving',
            style: TextStyle(
              color: Colors.black,
              fontFamily: 'OpenSans',
              fontSize: 25.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
      content: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          height: 300,
          width: MediaQuery. of(context). size. width,
          decoration: new BoxDecoration(
            borderRadius: new BorderRadius.all(new Radius.circular(50)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  'Time & Date',
                  style: TextStyle(
                    fontSize: 15
                  ),
                ),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 32.0),
                  child: Column(
                    children: [
                      TextField(
                        controller: timeDateBox,
                        keyboardType: TextInputType.text,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 5, top: 5, bottom: 5),
                            border: InputBorder.none
                        ),
                      ),
                      Divider(
                        color: Colors.deepOrange,
                        height: 1,
                        thickness: 1,
                        endIndent: 0,)
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  'Location',
                  style: TextStyle(
                      fontSize: 15
                  ),
                ),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 32.0),
                  child: Column(
                    children: [
                      TextField(
                        controller: locationBox,
                        keyboardType: TextInputType.text,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 5, top: 5, bottom: 5),
                            border: InputBorder.none
                        ),
                      ),
                      Divider(
                        color: Colors.deepOrange,
                        height: 1,
                        thickness: 1,
                        endIndent: 0,)
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  'Currant Hobo Reading',
                  style: TextStyle(
                      fontSize: 15
                  ),
                ),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 32.0),
                  child: Column(
                    children: [
                      TextField(
                        controller: hoboBox,
                        keyboardType: TextInputType.number,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 5, top: 5, bottom: 5),
                            border: InputBorder.none
                        ),
                      ),
                      Divider(
                        color: Colors.deepOrange,
                        height: 1,
                        thickness: 1,
                        endIndent: 0,)
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  TextEditingController timeDateBox = new TextEditingController();
  TextEditingController locationBox = new TextEditingController();
  TextEditingController hoboBox = new TextEditingController();

}
